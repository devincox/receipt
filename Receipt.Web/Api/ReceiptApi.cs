﻿using Suamere.Utilities.Monad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Trevo.Receipt.Web.Api
{
    public class ReceiptApiController : ApiController
    {
        private Core.IReceiptService _iReceiptSvc;
       
        public ReceiptApiController(Core.IReceiptService iReceiptSvc)
        {
            _iReceiptSvc = iReceiptSvc; 

        }
    }
}