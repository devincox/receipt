﻿using System;
using Trevo.MicroService;

namespace Trevo.Receipt.Web
{
    public partial class CompositionRoot
    {

        protected override object WhenInjectedInto(Type whenThis, Type isInjectedIntoThis)
        {
            throw new NotImplementedException();
        }

        private object InjectApiController(Type type, Type whenInjectedInto)
        {
            if (type == typeof(Api.ReceiptTestApiController)) return new Api.ReceiptTestApiController(GetService<Core.IReceiptTest>());
            if (type == typeof(Api.ReceiptApiController)) return new Api.ReceiptApiController(GetService<Core.IReceiptService>());

            return null;
        }

        private object InjectDomainService(Type type, Type whenInjectedInto)
        {
            if (type == typeof(Core.IReceiptService))
                return new ReceiptService(GetService<Data.Core.IReceiptRepo>());

            if (type == typeof(Core.IReceiptTest))
                return new ReceiptTest(GetService<Data.Core.IReceiptTestRepo>());

            return null;
        }

        private object InjectBackend(Type type, Type whenInjectedInto)
        {
            if (type == typeof(Data.Core.IReceiptRepo))
                return new Data.Repos.ReceiptRepo(GetService<Data.WrapExigo.IExigoWrapper>(), GetService<Logging.ILogService>(), _connectionString);

            if (type == typeof(Data.Core.IReceiptTestRepo))
                return new Data.Repos.ReceiptTestRepo(_connectionString);

            return null;
        }

        private object InjectThirdParty(Type type, Type whenInjectedInto)
        {
            //if (type == typeof(Data.WrapExigo.IExigoWrapper))
            //{
            //    ISettingManager settingManager = GetService<ISettingManager>();
            //    var exigoSetting = settingManager.GetAccessSettingFor(Setting.Models.Enums.SettingType.ExigoCredential, "Receipt");
            //    var exigoAdminSetting = settingManager.GetAccessSettingFor(Setting.Models.Enums.SettingType.ExigoCredential, "Receipt");

            //    return new Data.WrapExigo.ExigoWrapper(
            //          new Data.ExigoApi.ApiAuthentication
            //          {
            //              Company = "Trevo",
            //              LoginName = "svc_" + exigoSetting.Username,
            //              Password = exigoSetting.Passkey,
            //          }
            //        , new Data.ExigoAdminApi.ApiAuthentication
            //        {
            //            Company = "Trevo",
            //            LoginName = "svc_" + exigoAdminSetting.Username,
            //            Password = exigoAdminSetting.Passkey,
            //        }
            //        , GetService<Logging.ILogService>()
            //        , exigoSetting.AccessLocation
            //        , exigoAdminSetting.AccessLocation
            //    );
            //}

            return null;
        }
    }
}