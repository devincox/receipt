﻿using Suamere.Utilities.SimpleResolver;
using System;

namespace Trevo.Receipt.Web
{
    public partial class CompositionRoot : SuaCompositionRoot
    {
        protected static string _connectionString = null;

        protected override object GetService(Type type, Type whenInjectedInto)
        {
            // Leave this here for MicroService Cross-Cutting-Concerns
            if (MicroService.CompositionRoot.ControlsType(type)) return MicroService.CompositionRoot.GetService(type);

            if (_connectionString == null)
            {
                var conn = System.Configuration.ConfigurationManager.ConnectionStrings["contextConn"];
                if (conn != null && !string.IsNullOrWhiteSpace(conn.ConnectionString)) _connectionString = conn.ConnectionString;
                else _connectionString = (GetService<MicroService.ISettingManager>()).GetConnectionString("contextConn");
            }

            var apiController = InjectApiController(type, whenInjectedInto);
            if (apiController != null) return apiController;

            var domainService = InjectDomainService(type, whenInjectedInto);
            if (domainService != null) return domainService;

            var backend = InjectBackend(type, whenInjectedInto);
            if (backend != null) return backend;
            
            var other = InjectThirdParty(type, whenInjectedInto);
            if (other != null) return other;

            // Default (Should always be null, Web API and MVC Convention)
            return null;
        }
    }
}