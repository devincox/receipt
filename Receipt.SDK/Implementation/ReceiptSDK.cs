﻿using Suamere.Utilities.EndpointConnection.Contracts;

namespace Trevo.Receipt.SDK.Implementation
{
    public partial class ReceiptSDK : Core.IReceiptSDK
    {
        private IEndpointConnection _endpoint;
        private string _secretKey;

        public ReceiptSDK(IEndpointConnection endpoint, string secretKey)
        {
            _endpoint = endpoint;
            _secretKey = secretKey;
        }


        // Gets


        // Filters


        // Changes


    }
}