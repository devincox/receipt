﻿using Suamere.Utilities.EndpointConnection.Contracts;
using Suamere.Utilities.Monad;

namespace Trevo.Receipt.SDK.Implementation
{
    public class ReceiptTestSDK : Core.IReceiptTestSDK
    {
        private IEndpointConnection _endpoint;

        public ReceiptTestSDK(IEndpointConnection endpoint)
        {
            _endpoint = endpoint;
        }

        public Maybe<string> TestSuccessEndpoint()
        {
            try { return _endpoint.Get<string>("api/Success").ToMaybe(); }
            catch (System.Net.WebException ex) { return Maybe.Empty<string>(ex); }
        }
        public Maybe<string> TestFailEndpoint()
        {
            try { return _endpoint.Get<string>("api/Fail").ToMaybe(); }
            catch (System.Net.WebException ex) { return Maybe.Empty<string>(ex); }
        }
        public Maybe<string> TestExceptionEndpoint()
        {
            try { return _endpoint.Get<string>("api/Exception").ToMaybe(); }
            catch (System.Net.WebException ex) { return Maybe.Empty<string>(ex); }
        }
    }
}