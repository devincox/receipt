﻿using Suamere.Utilities.Monad;

namespace Trevo.Receipt.SDK.Core
{
    public interface IReceiptTestSDK
    {
        Maybe<string> TestSuccessEndpoint();
        Maybe<string> TestFailEndpoint();
        Maybe<string> TestExceptionEndpoint();
    }
}