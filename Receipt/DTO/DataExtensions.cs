﻿using Newtonsoft.Json;

namespace Trevo.Receipt.DTO
{
    public static class DataExtensions
    {
        public static T ConvertTo<T>(this object convertFrom)
        {
            var json = JsonConvert.SerializeObject(convertFrom);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}