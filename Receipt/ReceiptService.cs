﻿using Suamere.Utilities.Monad;
using System.Threading.Tasks;
using Trevo.Receipt.Core;
using Trevo.Receipt.Data.Core;
using Trevo.Receipt.DTO;

namespace Trevo.Receipt
{
    public class ReceiptService : IReceiptService
    {
        private IReceiptRepo _ReceiptRepo;

        public ReceiptService(IReceiptRepo ReceiptRepo)
        {
            _ReceiptRepo = ReceiptRepo;
        }


        // Gets (Monads and Tasks optional)


        // Filters (Monads and Tasks optional)


        // Synchronous Changes (Maybe<T> Required)


        // Async Changes (Task<Maybe<T>> Required)


    }
}