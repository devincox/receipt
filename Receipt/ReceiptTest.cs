﻿using System.Collections.Generic;
using Suamere.Utilities.Monad;
using Trevo.Receipt.Models;
using Trevo.Receipt.DTO;
using Trevo.Receipt.Data.Entities;
using System.Linq;
using Trevo.Receipt.Data.Core;

namespace Trevo.Receipt
{
    public class ReceiptTest : Core.IReceiptTest
    {
        private IReceiptTestRepo _testRepo;

        public ReceiptTest(IReceiptTestRepo testRepo)
        {
            _testRepo = testRepo;
        }

        public Maybe AddTestRecord(ReceiptTestModel Description)
        {
            _testRepo.AddDataTest(Description.ConvertTo<ReceiptTestEntity>());
            return Maybe.Success();
        }

        public Maybe<List<ReceiptTestModel>> GetTestRecords()
        {
            var tests = _testRepo.GetDataTests().Select(x => x.ConvertTo<ReceiptTestModel>()).ToList();
            return tests.ToMaybe();
        }
    }
}