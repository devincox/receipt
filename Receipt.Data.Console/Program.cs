﻿using System.Threading.Tasks;
using Trevo.Receipt.Data.Repos;

namespace Receipt.Data.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new ReceiptRepo(null, null, "");
            MainAsync(service).Wait();

            System.Console.ReadLine();
        }

        static async Task MainAsync(ReceiptRepo service)
        {
            await Task.FromResult(true); // Placeholder to appease the async gods.
            //var result = await service.DoSomething("something");
        }
    }
}