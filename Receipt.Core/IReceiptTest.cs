﻿using Suamere.Utilities.Monad;
using System.Collections.Generic;
using Trevo.Receipt.Models;

namespace Trevo.Receipt.Core
{
    public interface IReceiptTest
    {
        Maybe AddTestRecord(ReceiptTestModel Description);
        Maybe<List<ReceiptTestModel>> GetTestRecords();
    }
}