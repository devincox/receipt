﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trevo.Receipt.Data
{
    public static class Extensions
    {
        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        public static DateTime GetLastMillisecondOfMonth(this DateTime today)
        {
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month), 23, 59, 59);
            return endOfMonth;
        }

        public static DateTime GetFirstDayOfMonth(this DateTime today)
        {
            DateTime endOfMonth = new DateTime(today.Year, today.Month, 1);
            return endOfMonth;
        }
    }
}
