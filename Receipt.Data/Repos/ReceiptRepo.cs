﻿using Trevo.Receipt.Data.Core;

namespace Trevo.Receipt.Data.Repos
{
    public class ReceiptRepo : IReceiptRepo
    {
        private WrapExigo.IExigoWrapper _exigoWrapper;
        private Logging.ILogService _logger;
        private string _c = null;
        
        public ReceiptRepo(WrapExigo.IExigoWrapper exigoWrapper, Logging.ILogService logger, string connectionString)
        {
            _exigoWrapper = exigoWrapper;
            _logger = logger;
            _c = connectionString;
        }


        // Gets (Monads and Tasks optional)


        // Filters (Monads and Tasks optional)


        // Synchronous Changes (Maybe<T> Required)


        // Async Changes (Task<Maybe<T>> Required)


    }
}