﻿using System.Collections.Generic;
using System.Linq;
using Trevo.Receipt.Data.Entities;
using Trevo.Receipt.Data.Core;

namespace Trevo.Receipt.Data.Repos
{
    public class ReceiptTestRepo : IReceiptTestRepo
    {
        private string _c = null;

        public ReceiptTestRepo(string connectionString)
        {
            _c = connectionString;
        }

        public List<ReceiptTestEntity> GetDataTests()
        {
            using (ReceiptContext context = new ReceiptContext(_c))
            {
                return context.Set<ReceiptTestEntity>().ToList();
            }
        }

        public void AddDataTest(ReceiptTestEntity testEntity)
        {
            using (ReceiptContext context = new ReceiptContext(_c))
            {
                context.Tests.Add(testEntity);
                context.SaveChanges();
            }
        }
    }
}