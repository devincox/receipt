using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Trevo.Receipt.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ReceiptContext>
    {
        public Configuration() { AutomaticMigrationsEnabled = false; }

        protected override void Seed(ReceiptContext context)
        {
            context.Tests.AddOrUpdate(new Entities.ReceiptTestEntity
            { TestId = 1, TestDescription = "Zero One Two", CreatedDate = new System.DateTime(2010, 01, 02), });
            context.Tests.AddOrUpdate(new Entities.ReceiptTestEntity
            { TestId = 2, TestDescription = "Three Four Five", CreatedDate = new System.DateTime(2013, 04, 05), });

            // Use this to populate Enum tables with Values, if necessary
            context.ReceiptStatuses.SeedEnumValues<Entities.Enums.ReceiptStatusClass, Entities.Enums.ReceiptStatus>(x => new Entities.Enums.ReceiptStatusClass { ReceiptStatusID = (int)x, Description = x.ToString() });
        }
    }
    public static class ext
    {
        public static void SeedEnumValues<T, TEnum>(this IDbSet<T> dbSet, Func<TEnum, T> converter) where T : class => Enum.GetValues(typeof(TEnum))
                           .Cast<object>()
                           .Select(value => converter((TEnum)value))
                           .ToList()
                           .ForEach(instance => dbSet.AddOrUpdate(instance));
    }
}