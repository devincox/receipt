﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Trevo.Receipt.Data
{
    public partial class ReceiptContext : DbContext
    {
        static ReceiptContext()
        {
            Database.SetInitializer<ReceiptContext>(null);
        }

        public ReceiptContext(string conn) : base(conn)
        {
            Database.Log = s => Debug.WriteLine(s);
            Database.CommandTimeout = 300;
            Configuration.UseDatabaseNullSemantics = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Receipt");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Add(new CodeFirstStoreFunctions.FunctionsConvention<ReceiptContext>("dbo"));
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Core.Maps.ReceiptMap)));
            //modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Customer.Data.Core.Maps.CustomerMap)));
            //modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Order.Data.Core.Maps.OrderMap)));
            //modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Item.Data.Core.Maps.ItemMap)));
            //modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(Market.Data.Core.Maps.MarketMap)));
        }

        [DbFunction("ReceiptContext", "fnDatabaseFunctionName")]
        public IQueryable<Entities.ReceiptEntity> DatabaseFunctionName(int ReceiptID)
        {
            var param = new ObjectParameter("ReceiptID", ReceiptID);
            return (this as IObjectContextAdapter).ObjectContext.CreateQuery<Entities.ReceiptEntity>("[DatabaseFunctionName](@ReceiptID)", param);
        }
    }

    public class ReceiptContextFactory : IDbContextFactory<ReceiptContext>
    {
        public ReceiptContext Create()
        {
            return new ReceiptContext("");
        }
    }
}