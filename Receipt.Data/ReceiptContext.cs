﻿using Trevo.Receipt.Data.Entities;
using System.Data.Entity;

namespace Trevo.Receipt.Data
{
    public partial class ReceiptContext
    {
        public DbSet<ReceiptTestEntity> Tests { get; set; }
        public DbSet<ReceiptEntity> Receipts { get; set; }

        // Enum Class for Lookup Tables
        public DbSet<Entities.Enums.ReceiptStatusClass> ReceiptStatuses { get; set; }
    }
}