﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.WrapExigo
{
    public class ExigoWrapper : IExigoWrapper
    {
        //private ExigoApi.ApiAuthentication _exigoAuth;
        //private ExigoAdminApi.ApiAuthentication _exigoAdminAuth;
        //private Logging.ILogService _logService;
        //private string _apiUrl;
        //private string _adminUrl;

        //public ExigoWrapper(ExigoApi.ApiAuthentication exigoAuth, ExigoAdminApi.ApiAuthentication exigoAdminAuth, Logging.ILogService logService, string apiUrl, string adminUrl)
        //{
        //    _exigoAuth = exigoAuth;
        //    _exigoAdminAuth = exigoAdminAuth;
        //    _logService = logService;
        //    _apiUrl = apiUrl;
        //    _adminUrl = adminUrl;
        //}

        public async Task<bool> CreateReceiptsAsync(List<ReceiptEntity> Receipts)
        {
            await Task.FromResult(true); // Placeholder to appease the async gods.
            //using (var exigoClient = new ExigoApi.ExigoApiSoapClient("ExigoApiSoap", _apiUrl))
            //{
            //    //return await CreateReceiptsAsync(Receipts, exigoClient);
            //}
            return false;
        }

        //private async Task<bool> CreateReceiptsAsync(List<ReceiptEntity> Receipts, ExigoApi.ExigoApiSoapClient exigoClient)
        //{
        //    //var tasks = Receipts.Select(x =>
        //    //    exigoClient.CreateReceiptAsync(_exigoAuth, new ExigoApi.CreateReceiptRequest
        //    //    {
        //    //        //ReceiptID = x.ReceiptID,
        //    //        //OtherVariable = otherVariable,
        //    //    })
        //    //);
        //    //var errorResults = new List<string>();
        //    //(await Task.WhenAll(tasks)).Select(x => x.CreateReceiptResult.Result.Errors).ToList().ForEach(x => errorResults.AddRange(x));

        //    //if (errorResults.Count > 0)
        //    //{
        //    //    var stringBuilder = new StringBuilder();
        //    //    errorResults.ForEach(x => stringBuilder.AppendLine(string.Join(", ", x)));
        //    //    _logService.LogError(new Exception(""), stringBuilder.ToString());
        //    //    return false;
        //    //}

        //    return true;
        //}
    }
}