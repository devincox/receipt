﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.WrapExigo
{
    public interface IExigoWrapper
    {
        Task<bool> CreateReceiptsAsync(List<ReceiptEntity> Receipts);
    }
}