﻿using Suamere.Utilities.EndpointConnection.Executions;
using Trevo.Receipt.SDK.Core;
using Trevo.Receipt.SDK.Implementation;

namespace Receipt.SDK.Console
{
    class Program
    {
        static void Main()
        {

            var endpointConnector = new EndpointConnection("http://localhost:31130/");
            IReceiptSDK ReceiptSDK = new ReceiptSDK(endpointConnector, "generateSecretKey");
            IReceiptTestSDK testSDK = new ReceiptTestSDK(endpointConnector);

            TestReceipt(ReceiptSDK);

            System.Console.WriteLine("Press the Any Key to continue");
            System.Console.ReadKey();
        }

        static void TestReceipt(IReceiptSDK ReceiptService)
        {

        }
    }
}