﻿namespace Trevo.Receipt.Models.Enums
{
    public enum ReceiptStatus
    {
        Deleted = 0,
        Active = 1,
    }
}