﻿using System;

namespace Trevo.Receipt.Models
{
    public class ReceiptTestModel
    {
        public int TestId { get; set; }
        public string TestDescription { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}