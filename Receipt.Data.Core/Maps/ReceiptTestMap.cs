﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.Core.Maps
{
    public class ReceiptTestMap : EntityTypeConfiguration<ReceiptTestEntity>
    {
        public ReceiptTestMap()
        {
            //key  
            HasKey(k => k.TestId);

            //property  
            Property(p => p.TestId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.TestDescription);
            Property(p => p.CreatedDate).HasColumnType("datetime2").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            //table  
            ToTable("Receipt.Test");
        }
    }
}