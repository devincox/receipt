﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Trevo.Receipt.Data.Core.Maps
{
    public class ReceiptStatusMap : EntityTypeConfiguration<Entities.Enums.ReceiptStatusClass>
    {
        public ReceiptStatusMap()
        {
            //key
            HasKey(k => k.ReceiptStatusID);

            //property
            Property(p => p.ReceiptStatusID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(p => p.Description);

            //table  
            ToTable("Receipt.ReceiptStatus");
        }
    }
}