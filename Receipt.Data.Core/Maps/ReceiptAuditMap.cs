﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.Core.Maps
{
    public class ReceiptAuditMap : EntityTypeConfiguration<ReceiptAuditEntity>
    {
        public ReceiptAuditMap()
        {
            //key  
            HasKey(k => k.Id);

            //property  
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.Message);
            Property(p => p.MessageTemplate);
            Property(p => p.Level).HasColumnType("nvarchar").HasMaxLength(128);
            Property(p => p.TimeStamp).HasColumnType("datetimeoffset");
            Property(p => p.Exception);
            Property(p => p.Properties).HasColumnType("xml");
            Property(p => p.LogEvent);

            //table  
            ToTable("TrevoAudit.Receipt");
        }
    }
}
