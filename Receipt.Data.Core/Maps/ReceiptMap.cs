﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.Core.Maps
{
    public class ReceiptMap : EntityTypeConfiguration<ReceiptEntity>
    {
        public ReceiptMap()
        {
            //key  
            HasKey(k => k.ReceiptID);

            //relationships


            //Properties
            Property(p => p.ReceiptID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //Property(p => p.CreatedDate).HasColumnName("CreatedDate");

            //table  
            ToTable("Receipt.Receipt");
        }
    }
}