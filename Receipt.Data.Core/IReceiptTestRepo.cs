﻿using System.Collections.Generic;
using Trevo.Receipt.Data.Entities;

namespace Trevo.Receipt.Data.Core
{
    public interface IReceiptTestRepo
    {
        List<ReceiptTestEntity> GetDataTests();
        void AddDataTest(ReceiptTestEntity testEntity);
    }
}