﻿namespace Trevo.Receipt.Data.Entities.Enums
{
    public class ReceiptStatusClass
    {
        public int ReceiptStatusID { get; set; }
        public string Description { get; set; }
    }
}