﻿namespace Trevo.Receipt.Data.Entities.Enums
{
    public enum ReceiptStatus
    {
        Deleted = 0,
        Active = 1,
    }
}