﻿using System;

namespace Trevo.Receipt.Data.Entities
{
    public class ReceiptTestEntity
    {
        public int TestId { get; set; }
        public string TestDescription { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}